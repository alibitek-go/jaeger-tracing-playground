package main

import (
	"log"

	"gitlab.com/alibitek-go/tracing/jaeger-quickstart/ops"
	"gitlab.com/alibitek-go/tracing/jaeger-quickstart/tracing"
)

func main() {
	closer, err := tracing.Initialize()
	if err != nil {
		log.Fatal("Cannot initialize tracing")
	}
	defer closer.Close()

	ops.HelloLocal("Local")

	api := ops.NewAPI("http://localhost:8080")
	log.Println(ops.HelloRemote("Remote", api))
}
