package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"gitlab.com/alibitek-go/tracing/jaeger-quickstart/tracing"
)

func randFloat(min, max float64) float64 {
	return min + rand.Float64()*(max-min)
}

func main() {
	rand.Seed(time.Now().UnixNano())

	closer, err := tracing.Initialize()
	if err != nil {
		log.Fatal("Cannot initialize tracing")
	}
	defer closer.Close()

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		tracer := opentracing.GlobalTracer()

		// Extract the context from the headers
		spanCtx, _ := tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
		serverSpan := tracer.StartSpan("hello-server", ext.RPCServerOption(spanCtx))
		defer serverSpan.Finish()

		time.Sleep(time.Duration(randFloat(0, 5)) * time.Second)

		nameValues, ok := r.URL.Query()["name"]
		if !ok || len(nameValues[0]) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		name := nameValues[0]
		w.Write([]byte(fmt.Sprintf("Hello, %s!", name)))
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}
