package ops

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

// HelloLocal operation says hello in a nice way
func HelloLocal(name string) string {
	tracer := opentracing.GlobalTracer()
	span := tracer.StartSpan("HelloLocal",
		opentracing.Tag{Key: "name", Value: name},
	)
	defer span.Finish()

	return messageFormatter(span, "Hello, ", name, "! Comment ça va?")
}

func messageFormatter(parentSpan opentracing.Span, msg ...string) string {
	// Create a Child Span. Note that we're using the ChildOf option.
	tracer := opentracing.GlobalTracer()
	childSpan := tracer.StartSpan(
		"messageFormatter",
		opentracing.ChildOf(parentSpan.Context()),
		opentracing.Tag{Key: "number_of_messages", Value: len(msg)},
		opentracing.Tag{Key: "message", Value: msg},
	)
	defer childSpan.Finish()

	sb := strings.Builder{}
	for _, m := range msg {
		sb.WriteString(m)
	}
	return sb.String()
}

// API encapsulates the actions that can be performed remotely
type API interface {
	Hello(name string) (string, error)
}

type apiImpl struct {
	baseURL string
}

// NewAPI creates a new API instance
func NewAPI(baseURL string) API {
	return &apiImpl{
		baseURL: baseURL,
	}
}

func (api *apiImpl) Hello(name string) (string, error) {
	tracer := opentracing.GlobalTracer()
	clientSpan := tracer.StartSpan("hello-client")
	defer clientSpan.Finish()

	url := fmt.Sprintf("%s/hello?name=%s", api.baseURL, url.QueryEscape(name))
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", fmt.Errorf("Cannot construct HTTP GET request for URL: %s, reason: %w", url, err)
	}

	// Set some tags on the clientSpan to annotate that it's the client span. The additional HTTP tags are useful for debugging purposes.
	ext.SpanKindRPCClient.Set(clientSpan)
	ext.HTTPUrl.Set(clientSpan, url)
	ext.HTTPMethod.Set(clientSpan, "GET")

	// Get traces across service boundaries, we propagate context by injecting the context into http headers
	// Inject the client span context into the headers
	tracer.Inject(clientSpan.Context(), opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(req.Header))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", fmt.Errorf("Cannot make HTTP GET request to server: %s, reason: %w", url, err)
	}

	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Cannot read response body", err)
	}

	return string(data), nil
}

// HelloRemote says hello from a remote server
func HelloRemote(name string, api API) string {
	result, _ := api.Hello(name)
	return result
}
