package ops

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type apiMock struct{}

func (m *apiMock) Hello(name string) (string, error) {
	return fmt.Sprintf("Hello, %s!", name), nil
}

func serverMock() *httptest.Server {
	handler := http.NewServeMux()
	handler.HandleFunc("/hello", helloMockHandler)
	return httptest.NewServer(handler)
}

func helloMockHandler(w http.ResponseWriter, r *http.Request) {
	nameValues, ok := r.URL.Query()["name"]
	if !ok || len(nameValues[0]) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	name := nameValues[0]
	w.Write([]byte(fmt.Sprintf("Hello remotely, %s!", name)))
}

func TestHelloLocal(t *testing.T) {
	actual := HelloLocal("Test")
	if !strings.Contains(actual, "Test") {
		t.Errorf("Saying hello failed! %s", actual)
	}
}

func TestHelloRemoteWithMock(t *testing.T) {
	mock := &apiMock{}
	actual := HelloRemote("Test", mock)
	if !strings.Contains(actual, "Test") {
		t.Errorf("Saying hello remotely failed! %s", actual)
	}
}

func TestHelloRemoteWithMockedServer(t *testing.T) {
	serverMock := serverMock()
	defer serverMock.Close()

	api := NewAPI(serverMock.URL)
	actual := HelloRemote("Test", api)
	if !strings.Contains(actual, "Test") {
		t.Errorf("Saying hello remotely failed! %s", actual)
	}
}
