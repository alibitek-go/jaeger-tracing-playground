# Distributed tracing playground

## Jaeger
https://opentracing.io/guides/golang/quick-start/

### Usage
```
docker run -d -p 6831:6831/udp -p 16686:16686 jaegertracing/all-in-one:latest
go run downstream/downstream.go &
go run upstream/upstream.go
xdg-open http://localhost:16686
```